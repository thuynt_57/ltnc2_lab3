/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 * Description : Ex1 of lab3
 * Task		   :  Xay dung ham xu ly tren xau, lam viec tren C-String (char*)
					a) H�m chuann h�a t�n, v� du dau v�o " lE vAn AN "  --> "Le Van An"
					b) H�m dem so tu trong mot x�u, c�c tu c�ch nhau boi mot hoac nhieu dau c�ch
					c) H�m tra ve danh sach cac tu
					vi du dau v�o " cong nghe thong tin " thi dau ra tra ve 4 tu (c� co the dung mang, dung danh sach lien ket...) l� "cong", "nghe", "thong", "tin". 			     
 * Reference   :
 * Date        : 18/4/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#define MAX 100
#define max 10

using namespace std;
 

void deleteLeftSpace( char *str ) {   
    int l = strlen(str);
    // If get the space in the beginning of the string, shift right
    while (str[0] == ' ') {
        for(int i = 0; i < l; i++)
            str[i] = str[i + 1];  
    }
}
 
void deleteRightSpace( char *str ) {
    int l = strlen(str);
    // If get the space in the end of the string, shift left
    while ( str[l-1] == 32 ) {
        str[l-1] = 0;
        l--;
    }
}
 
void standardize( char *str ) {
    int l = strlen(str);
    deleteLeftSpace(str);
    deleteRightSpace(str);
    // Delete spaces in the middle  of the string
    for ( int i = 0; i < l; i++) {
        // If get 2 consecutive spaces, shift right 
        while( ( str[i] == 32 ) && ( str[i+1] == 32 )) {
            for ( int j = i; j < l; j++ ) {
                str[j] = str[j+1];
            }
        }
    }
}
 
// count the num of word
int count( char *str ) {   
	char *temp;  // We just want to count, not expect any change with our string, so we use temp var for this stuff 
	temp = str;
	standardize(temp);
    int l = strlen(temp);    
    int num = 0;
    // After standardize, if get empty string, return 0
    if (temp[0] == 0) {
        return num;
    }
    // Num of words = num of spaces + 1
    for ( int i = 0; i < l; i++ ) {
        if ( temp[i] == 32 ) {
            num++;
        }
    }
    return ( num + 1 );
}

// standardize name: all first letter of words are capital, others are normal
void standardizeName( char *str ){
	standardize(str);
	for ( int i = 0; i < strlen(str); i++ ) {
		if ( i == 0 || str[i-1] == 32 ) {
			str[i] = toupper(str[i]);
		}
		else {
			str[i] = tolower(str[i]);
		}
	}
}

// get a word, which begin at 'first' index and end at 'last' index, from a sennamece
char* getWord( char* str, int first, int last ){
    char* word = new char[max];
    for ( int i = first; i <= last; i++ ){
        word[i-first] = str[i];
    }
    word[last-first+1]='\0';
    return word;
}

// get all index of words from a sennamece
int* getIndexOfWords( char *p ){
    int *indexs = new int[MAX];
	int t = 0;
    int first = 0;

    for ( int i = 0; i <= strlen(p); i++ ){
        if ( p[i] == ' ' || p[i] == '\0' ){
            indexs[t] = first; t++;
            indexs[t] = i; t++;
            first = i+1;
        }
    }
	return indexs;
}


 
int main(){
    char *str1, *name, *str2;
    
    // test a
    str1 = new char[100];
    cout << "1. Enter a string: ";
    gets(str1);
    int numOfWord = count(str1);
    cout << "Num of word is: " << numOfWord << endl;
    
    // test b 
    name = new char[MAX];
    cout << "2. Enter a name: ";
    gets(name);
    standardizeName(name);
    cout << "Name after standardize: " << name << endl;
    
    // test c
    str2 = new char[MAX];
    cout << "3. Enter a string: "; 
    gets(str2);
    int *index = new int[count(str2)*2];
    index = getIndexOfWords(str2);
    
    int t = 0;
	cout << "Word of string: ";
    for ( int i = 0; i < count(str2); i++ ) {
    	cout << i + 1 << ". " << getWord(str2, index[t], index[t+1]);
    	cout << "   ";
    	t+= 2;
    }
    
    return 0;
}
