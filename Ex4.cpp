/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex4 of lab3
 * Task		   :  Su dung danh sach lien ket viet chuong trinh quan ly sinh vien cua mot low theo cac chuc nang sau
 					1. Nhap danh sach
					2. Hien thi ra man hinh danh sach
					3. Sap xep theo ho name
					4. Sap xep theo score
					5. Tim kiem tat ca cac sinh vien co score tu a den b
					6. Tim kiem tat ca cac sinh vien co name la name nhap vao tu ban phim (lay tat ca sinh vien co ho name chua name nhap vao) 
 * Reference   :
 * Date        : 23/4/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#define MAX 100
#define max 10

using namespace std;

#include <iostream>
using namespace std;
struct Student{
    string name;
    int score;
    Student *next;
};
Student *head=NULL;
Student *tail=NULL;

void input(){
    string name;
    int score;
    Student *p = new Student();
    
    cout << "\nName: ";
    cin.ignore(1);
    getline(cin,name);
    cout << "Score: ";
    cin >> score;
    p->name = name;
    p->score = score;
    
    if ( head == NULL && tail == NULL){
        head = p;
        tail = p;
    }else{
        tail->next = p;
        tail = p;
    }
}

void print(){
    Student *p;
    p = head;
    cout << "-----------List of students------------" << endl;
    while (p != NULL) {
        cout << p->name << "  " << p->score << endl;
        p = p->next;
    }
    cout << endl;
}

void Swap(Student *a, Student *b){
    string name;
    int score;
    name = a->name;
    a->name = b->name;
    b->name = name;
    score = a->score;
    a->score = b->score;
    b->score = score;
}
string lastName(string a){
    for( int i = a.length()-1 ;i >= 0; i-- )
        if( a[i] == ' ' )
            return a.substr( i+1, a.length() - i );
    
    return a;
}
void sortByName(){
    Student *p;
    bool checked = true;
    while( checked ){
        checked = false;
        p = head;
        while( p->next != NULL ){
            string name1 = lastName(p->name);
            string name2 = lastName(p->next->name);
            if( name1.compare(name2) > 0){
                Swap(p, p->next);
                checked = true;
            }
            p = p->next;
        }
    }
}
void sortByScore(){
    Student *p;
    bool checked=true;
    while( checked ){
        checked = false;
        p = head;
        while(p->next != NULL) {
            if(p->score < p->next->score){
                Swap(p, p->next);
                checked=true;
            }
            p=p->next;
        }
    }
}
void findByScore(){
    Student *p;
    int a,b;
    cout<"Enter a range of score u wish to find: \n";
    cin >> a >> b;
    bool flag = false;
    p = head;
    while( p != NULL ){
        if ( a <= p->score && p->score <= b) {
        	flag = true;
            cout << p->name << "  " << p->score << endl;
        }
        p = p->next;
    }
    if( flag == false) cout << "No one found" << endl;
}

void findByName(){
    string name;
    bool flag = false;
    Student *p;
    p = head;
    cout << "Enter the name u wish to find: ";
    cin.ignore(1);
    getline(cin,name);
    cin.ignore(1);
    while(p != NULL ){
        if( name.compare(lastName(p->name)) == 0 ){
            cout << p->name << "  " << p->score << endl;
            flag = true;
        }
        p = p->next;
    }
    if ( flag == false ) cout << "No one found\n\n";

}
int main(){
    int menu = 7;

    while (menu) {
    	cout << "0. Exit" << endl;
	    cout << "1. Enter the list" << endl;
	    cout << "2. Print the list" << endl;
	    cout << "3. Sort by name" << endl;
	    cout << "4. Sort by score" << endl;
	    cout << "5. Find student whose score in range of a to b" << endl;
	    cout << "6. Find by name" << endl;
	    
        cin >> menu;
        
        switch(menu){
        	case 0:
        		break;
            case 1:
                input();
                break;
            case 2:
                print();
                break;
            case 3:
                sortByName();
                print();
                break;
            case 4:
                sortByScore();
                print();
                break;
            case 5:
                findByScore();
                break;
            case 6:
                findByName();
                break;
        }

    }
    return 0;
}

