/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex3 of lab3
 * Task		   :  Viet ham tim xem xau con xuat hien bao nhieu lan trong xau da cho	     
 * Reference   :
 * Date        : 23/4/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#define MAX 100
#define max 10

using namespace std;
 
int count( string str, string subStr ){
    string temp;
    int count = 0;
    int i = 0;
    while( i < ( str.length() - subStr.length() + 1 ) ){
        temp = str.substr(i ,subStr.length());
        if( temp.compare(subStr) == 0) {
            count++;
            i += subStr.length();
        }
        else i++;
    }
    return count;

}

int main(){
    string str;
    string subStr;

    cout << "Enter your string: ";
    getline(cin, str); 
    cout << "Enter your sub string: ";
    getline(cin, subStr);
	cout << "Your sub string appears " << count(str, subStr) << " time(s) in your string." << endl;

    return 0;
}
